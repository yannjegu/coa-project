package fr.istic.methodInvocation;

import fr.istic.clientServant.Captor;
import fr.istic.clientServant.implementation.Value;

import java.util.concurrent.Callable;

/**
 * GetValue class is {@link Callable}.
 * It has a {@link GetValue#call()} method which return a {@link Value}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class GetValue implements Callable {
    private Captor captor;

    /**
     * Constructor.
     * @param captor the reference to the captor.
     */
    public GetValue(Captor captor) {
        this.captor = captor;
    }

    /**
     * Call method which prevent the captor that the {@link fr.istic.clientServant.Displayer} has received a value.
     * @return a {@link Value} of the {@link Captor}.
     * @throws Exception if something bad happens.
     */
    @Override
    public Value call() throws Exception {
        return this.captor.getValue();
    }
}
