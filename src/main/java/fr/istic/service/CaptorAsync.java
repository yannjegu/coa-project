package fr.istic.service;

import fr.istic.clientServant.implementation.Value;

import java.util.concurrent.Future;

/**
 * Interface CaptorAsync.
 * This interface is only known by {@link fr.istic.clientServant.Displayer}s.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
interface CaptorAsync {
    /**
     * Method which return a {@link Future<Value>}.
     * @return the Future of type {@link Value}.
     */
    Future<Value> getValue();
}
