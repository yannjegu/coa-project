package fr.istic.service;

import fr.istic.clientServant.Captor;
import fr.istic.clientServant.Displayer;
import fr.istic.clientServant.implementation.Value;

import java.util.concurrent.*;

/**
 * Implements {@link CaptorAsync} and {@link ObserverCaptorAsync}.
 * The Canal will do the link (the proxy) between {@link Captor} and {@link Displayer}.
 * This class manipulates the {@link ScheduledExecutorService} to send data to the Captor and the Displayer.
 */
public class Canal implements CaptorAsync, ObserverCaptorAsync {
    private ScheduledExecutorService scheduler;
    private Displayer displayer;
    private Captor captor;

    /**
     * Constructor.
     * @param displayer the reference to the {@link Displayer}.
     * @param captor the reference to the {@link Captor}.
     * @param scheduler the {@link ScheduledExecutorService}.
     */
    public Canal(Displayer displayer, Captor captor, ScheduledExecutorService scheduler) {
        this.scheduler = scheduler;
        this.displayer = displayer;
        this.captor = captor;
    }

    /**
     * The scheduler will schedule the {@link Captor#getValue()}.
     * @return a {@link ScheduledFuture<Value>}.
     */
    public ScheduledFuture<Value> getValue() {
        long delay = (long) (Math.random() * 1000);
        return this.scheduler.schedule(() -> captor.getValue(), delay, TimeUnit.MILLISECONDS);
    }

    /**
     * The scheduler will schedule the {@link Displayer#update()}.
     * @return a {@link ScheduledFuture<>}.
     */
    public ScheduledFuture<?> update() {
        long delay = (long) (Math.random() * 1000);
        return this.scheduler.schedule(() -> displayer.update(), delay, TimeUnit.MILLISECONDS);
    }

    /**
     * Getter of {@link Captor}.
     * @return the captor.
     */
    public Captor getCaptor() {
        return this.captor;
    }

    public Displayer getDisplayer() {
        return this.displayer;
    }
}
