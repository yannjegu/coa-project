package fr.istic.service;

import fr.istic.clientServant.Displayer;

import java.util.concurrent.Future;

/**
 * Interface ObserverCaptorAsync.
 * This interface is only known by {@link fr.istic.clientServant.Captor}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public interface ObserverCaptorAsync {
    /**
     * Return a {@link Future}.
     * @return a Future with a any type.
     */
    Future<?> update();

    /**
     * Getter of {@link Displayer}.
     * @return the Displayer.
     */
    Displayer getDisplayer();
}
