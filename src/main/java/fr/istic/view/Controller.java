package fr.istic.view;

import fr.istic.clientServant.Displayer;
import fr.istic.clientServant.implementation.Value;
import fr.istic.strategy.Algorithm;
import fr.istic.strategy.TypeBroadcast;

import javafx.fxml.FXML;
import javafx.scene.control.*;

import javafx.event.ActionEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JavaFX controller.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class Controller {

    @FXML
    private Label display1;
    @FXML
    private Label display2;
    @FXML
    private Label captor;
    @FXML
    private RadioButton atomicButton;
    @FXML
    private RadioButton sequentialButton;
    @FXML
    private RadioButton timestampButton;

    private MyLabel labelDisplay1;
    private MyLabel labelDisplay2;
    private MyLabel labelCaptor;

    private Map<Integer, MyLabel> myLabelMap;
    private List<Displayer> displayers;
    private Algorithm algorithm;


    /**
     * Same purpose as the Constructor.
     * We implement a method instead because the Controller is instantiated by FXML.
     */
    @FXML
    void initialize() {
        final ToggleGroup group = new ToggleGroup();
        labelCaptor = new LabelDecorator(captor);
        labelDisplay1 = new LabelDecorator(display1);
        labelDisplay2 = new LabelDecorator(display2);
        atomicButton = new RadioButton();
        atomicButton.setToggleGroup(group);
        atomicButton.setSelected(true);
        sequentialButton = new RadioButton();
        sequentialButton.setToggleGroup(group);
        timestampButton = new RadioButton();
        timestampButton.setToggleGroup(group);
        myLabelMap = new HashMap<>();
        displayers = new ArrayList<>();
        myLabelMap.put(1, labelDisplay1);
        myLabelMap.put(2, labelDisplay2);
        algorithm = new Algorithm();
    }

    /**
     * Update visually the {@link Label} of the captor.
     * @param captorValue the new value of the captor.
     */
    @FXML
    public void updateCaptor(Value captorValue){
        labelCaptor.updateCaptorValue(captorValue);
    }

    /**
     * Update visually the {@link Label} of a displayer.
     * @param id the id of displayer to update.
     * @param displayValue the new value of the displayer.
     */
    @FXML
    public void updateValueDisplay(int id, Value displayValue){
        MyLabel currentLabel = myLabelMap.get(id);
        if(currentLabel != null) {
            currentLabel.updateDisplayValue(displayValue);
        }
    }

    /**
     * Getter of the label of the captor.
     * @return the label of the captor.
     */
    @FXML
    public Label getCaptor() {
        return captor;
    }

    /**
     * Add a displayer to the list of the displayers.
     * @param displayer the displayer to add.
     */
    @FXML
    void addDisplayer (Displayer displayer){
        this.displayers.add(displayer);
    }

    @FXML
    public void changeTypeAtomic(ActionEvent event){
        algorithm.choiceAlgo(TypeBroadcast.Atomic);
        System.out.println("toto");
    }

    @FXML
    public void changeTypeSequential(ActionEvent event){
        algorithm.choiceAlgo(TypeBroadcast.Sequential);
        System.out.println("seque");
    }

    @FXML
    public void changeTypeTimestamp(ActionEvent event){

        algorithm.choiceAlgo(TypeBroadcast.Timestamp);
        System.out.println("time");
    }

}
