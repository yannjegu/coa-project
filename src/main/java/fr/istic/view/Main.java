package fr.istic.view;

import fr.istic.clientServant.Captor;
import fr.istic.clientServant.Displayer;
import fr.istic.clientServant.implementation.CaptorImpl;
import fr.istic.clientServant.implementation.DisplayerImpl;
import fr.istic.strategy.Algorithm;
import fr.istic.strategy.BroadcastAlgorithm;
import fr.istic.strategy.TypeBroadcast;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Launcher class for JavaFX application.
 * Extends {@link Application}.
 */
public class Main extends Application {

    /**
     * Method which will set up all the things for the application.
     * @param primaryStage the main {@link Stage}.
     * @throws Exception if something wrong happens.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
        Parent root = loader.load();
        System.out.println(getClass().getClassLoader().getResource("sample.fxml"));
        primaryStage.setTitle("fr.istic.Observer");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setOnCloseRequest(t1 -> System.exit(0));
        //loader.setController(new Controller());
        Algorithm algo = new Algorithm();
        BroadcastAlgorithm algorithm = algo.choiceAlgo(TypeBroadcast.Atomic);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);

        Controller ctrl = loader.getController();
        ctrl.initialize();
        Captor c = new CaptorImpl(algorithm, ctrl);
        
        Displayer d1 = new DisplayerImpl(1, c, scheduler, ctrl);
        Displayer d2 = new DisplayerImpl(2, c, scheduler, ctrl);
        ctrl.addDisplayer(d1);
        ctrl.addDisplayer(d2);
        c.attach(d1.getCanal());
        c.attach(d2.getCanal());
        Thread t = new Thread(c);
        t.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
