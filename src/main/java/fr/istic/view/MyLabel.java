package fr.istic.view;

import fr.istic.clientServant.implementation.Value;

/**
 * Interface MyLabel.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
interface MyLabel {
    /**
     * Update visually the value of the captor.
     * @param captorVal the new value of the captor.
     */
    void updateCaptorValue(Value captorVal);

    /**
     * Update visually the value of the displayer.
     * @param displayVal the new value of the displayer.
     */
    void updateDisplayValue(Value displayVal);
}
