package fr.istic.view;

import fr.istic.clientServant.implementation.Value;
import javafx.scene.control.Label;

/**
 * Implementation of interface {@link MyLabel}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
class LabelDecorator implements MyLabel {

    private Label label;

    /**
     * Constructor.
     * @param lab the label.
     */
    LabelDecorator (Label lab){
        label = lab;
    }

    @Override
    public void updateCaptorValue(Value captorVal) {
        label.setText(Integer.toString(captorVal.getValue()));
    }

    @Override
    public void updateDisplayValue(Value displayVal) {
        label.setText(Integer.toString(displayVal.getValue()));
    }
}
