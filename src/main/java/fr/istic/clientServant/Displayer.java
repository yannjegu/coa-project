package fr.istic.clientServant;

import fr.istic.service.Canal;

import java.util.concurrent.ScheduledExecutorService;

/**
 * Interface Displayer.
 * The displayer is in charge to display values sent by the {@link Captor}.
 * Its implementation is {@link fr.istic.clientServant.implementation.DisplayerImpl}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public interface Displayer {
    /**
     * Retrieves the value sent by the {@link Canal}.
     * This value is scheduled through {@link fr.istic.methodInvocation.GetValue} by the {@link ScheduledExecutorService}.
     * Then it will update the JavaFX {@link javafx.scene.control.Label}.
     */
    void update();

    /**
     * Getter of the {@link Canal}.
     * @return the channel.
     */
    Canal getCanal();

    /**
     * This method is useful only when broadcast algorithm is {@link fr.istic.strategy.implementation.AtomicBroadcast}.
     * It will test if the current Displayer has received the new value sent by the {@link Canal}.
     * @return true if it has the new value, false otherwise.
     */
    boolean isCalled();

    void setCalled(boolean isCalled);

    /**
     * Getter of the {@link Captor}.
     * @return the Captor.
     */
    Captor getCaptor();
}
