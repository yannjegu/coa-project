package fr.istic.clientServant.implementation;


import java.security.InvalidParameterException;

/**
 * Object representing the data sent and received by {@link fr.istic.clientServant.Captor} and {@link fr.istic.clientServant.Displayer}.
 * It has a value which is a counter and a timestamp (which is also a counter for the moment).
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class Value {
    private int timestamp;
    private int value;

    /**
     * Constructor.
     * @param value the value.
     * @param timestamp the timestamp, used only for broadcast algorithm {@link fr.istic.strategy.implementation.TimestampBroadcast}.
     */
    public Value(int value, int timestamp) {
        this.timestamp = timestamp;
        this.value = value;
    }

    /**
     * Getter of the value.
     * @return the value.
     */
    public int getValue(){
        return value;
    }

    /**
     * Setter of the value.
     * @param value the value.
     */
    public void setValue(int value)  {
        this.value = value;
    }

    /**
     * Getter of the timestamp.
     * @return the timestamp.
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * Setter of the timestamp.
     * @param timestamp the timestamp.
     */
    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
