package fr.istic.clientServant.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;
import fr.istic.view.Controller;
import javafx.application.Platform;

import java.util.*;

/**
 * Implementation of interface {@link Captor}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class CaptorImpl implements Captor {
    private List<ObserverCaptorAsync> channels;
    private BroadcastAlgorithm broadcastAlgorithm;
    private Controller controller;
    private Value value;
    private Timer timer;

    /**
     * Constructor.
     * A broadcast algorithm must be defined when instantiating a {@link Captor}.
     * @param algo the broadcast algorithm to apply to the captor.
     * @param controller a reference to the JavaFX controller.
     */
    public CaptorImpl(BroadcastAlgorithm algo, Controller controller) {
        this.value = new Value(0, 0);
        this.channels = new ArrayList<>();
        this.broadcastAlgorithm = algo;
        this.controller = controller;
        this.timer = new Timer();
    }

    @Override
    public Value getValue() {
        return value;
    }

    public void tick() {
        value.setValue(value.getValue() + 1);
        Platform.runLater(() -> controller.updateCaptor(value));
        broadcastAlgorithm.configure(this, channels);
        broadcastAlgorithm.execute();
    }

    public void attach(ObserverCaptorAsync o) {
        channels.add(o);
    }

    public void detach(ObserverCaptorAsync o) {
        channels.remove(o);
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tick();
        }
    }
}
