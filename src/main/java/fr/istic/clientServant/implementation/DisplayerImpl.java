package fr.istic.clientServant.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.clientServant.Displayer;
import fr.istic.service.Canal;
import fr.istic.view.Controller;
import javafx.application.Platform;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

/**
 * Implementation of interface {@link Displayer}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class DisplayerImpl implements Displayer {
    private Captor captor;
    private Canal canal;
    private Controller controller;
    private int id;
    private boolean isCalled = false;

    /**
     * Constructor.
     * Creates also the {@link Canal}.
     * @param captor the {@link Captor}.
     * @param scheduler the {@link ScheduledExecutorService}.
     * @param controller the {@link Controller} JavaFX.
     */
    public DisplayerImpl(int id, Captor captor, ScheduledExecutorService scheduler, Controller controller) {
        this.id = id;
        this.captor = captor;
        this.canal = new Canal(this, captor, scheduler);
        this.controller = controller;
    }

    @Override
    public void update() {
        isCalled = false;
        ScheduledFuture<Value> f = this.canal.getValue();
        Value value = null;
        try {
            value = f.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Value finalValue = value;
        /*
         * We must call Platform.runLater when JavaFX UI is changing.
         * Because UI is changing outside the main JavaFX Thread.
         */
        Platform.runLater(() -> controller.updateValueDisplay(id, finalValue));
        isCalled = true;
    }

    @Override
    public Canal getCanal() {
        return this.canal;
    }

    @Override
    public boolean isCalled() {
        return isCalled;
    }

    public void setCalled(boolean isCalled) {
        this.isCalled = isCalled;
    }

    @Override
    public Captor getCaptor() {
        return captor;
    }
}
