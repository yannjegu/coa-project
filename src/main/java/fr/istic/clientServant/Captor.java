package fr.istic.clientServant;

import fr.istic.clientServant.implementation.Value;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Interface Captor.
 * The captor is a counter which will be incremented at each tick.
 * Its implementation {@link fr.istic.clientServant.implementation.CaptorImpl}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public interface Captor extends Runnable {
    /**
     * Retrieves the current value of the captor.
     *
     * @return the current value
     */
    Value getValue();

    /**
     * Simulates a "tick" of a clock.
     * At each tick, the value of the captor is incremented on this method.
     * Uses {@link Timer} and {@link TimerTask} to manage rhythm of ticks.
     * Calls {@link BroadcastAlgorithm#configure(Captor, List)} to set up captor and channels.
     * Then calls {@link BroadcastAlgorithm#execute()} which will perform specific actions according to broadcast algorithm.
     */
    void tick();

    /**
     * Attaches an {@link ObserverCaptorAsync} on this captor.
     *
     * @param o the channel to attach on.
     */
    void attach(ObserverCaptorAsync o);

    /**
     * Detaches an {@link ObserverCaptorAsync} on this captor.
     *
     * @param o the channel to detach on.
     */
    void detach(ObserverCaptorAsync o);
}
