package fr.istic.strategy;

/**
 * Created by jbrulez on 15/03/2017.
 */
public enum TypeBroadcast {
    Atomic, Sequential, Timestamp
}
