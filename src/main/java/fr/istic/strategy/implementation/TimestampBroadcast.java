package fr.istic.strategy.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;
import fr.istic.view.Controller;

import java.util.List;

/**
 * Implementation of interface {@link BroadcastAlgorithm}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class TimestampBroadcast implements BroadcastAlgorithm {
    private Captor captor;
    private List<ObserverCaptorAsync> channels;

    public void configure(Captor captor, List<ObserverCaptorAsync> channels) {
        this.captor = captor;
        this.channels = channels;
    }

    public void execute() {
        this.captor.getValue().setTimestamp(this.captor.getValue().getValue());
        System.out.println(this.captor.getValue().getTimestamp());
        for(ObserverCaptorAsync c : this.channels) {
            c.update();
        }
    }
}
