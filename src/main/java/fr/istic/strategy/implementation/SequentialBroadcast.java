package fr.istic.strategy.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.clientServant.Displayer;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of interface {@link BroadcastAlgorithm}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class SequentialBroadcast implements BroadcastAlgorithm {
    private Captor captor;
    private List<ObserverCaptorAsync> channels;

    public void configure(Captor captor, List<ObserverCaptorAsync> channels) {
        this.captor = captor;
        this.channels = channels;
    }

    public void execute() {
        List<Displayer> displayers = new ArrayList<>();
        for(ObserverCaptorAsync c : this.channels) {
            displayers.add(c.getDisplayer());
        }
        this.channels.forEach(ObserverCaptorAsync::update);
    }
}
