package fr.istic.strategy.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;

import java.util.List;

/**
 * Implementation of interface {@link BroadcastAlgorithm}.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public class AtomicBroadcast implements BroadcastAlgorithm {
    private Captor captor;
    private List<ObserverCaptorAsync> channels;

    public void configure(Captor captor, List<ObserverCaptorAsync> channels) {
        this.captor = captor;
        this.channels = channels;
    }

    public void execute() {
        boolean isFinished = false;
        for (ObserverCaptorAsync channel : this.channels) {
            channel.getDisplayer().setCalled(false);
        }
        this.channels.forEach(ObserverCaptorAsync::update);
        while(!isFinished) {
            isFinished = areDisplayersUpdated();
        }
    }

    private boolean areDisplayersUpdated() {
        for (ObserverCaptorAsync c : this.channels) {
            if(!c.getDisplayer().isCalled()) {
                return false;
            }
        }
        return true;
    }
}
