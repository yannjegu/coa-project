package fr.istic.strategy;

import fr.istic.strategy.implementation.AtomicBroadcast;
import fr.istic.strategy.implementation.SequentialBroadcast;
import fr.istic.strategy.implementation.TimestampBroadcast;

import java.security.InvalidParameterException;

/**
 * Created by jbrulez on 15/03/2017.
 */
public class Algorithm {
    private BroadcastAlgorithm broadcastAlgorithm;

    public Algorithm (){
    }

    public BroadcastAlgorithm choiceAlgo(TypeBroadcast typeBroadcast){
        broadcastAlgorithm = null;

        switch (typeBroadcast){
            case Atomic:
                broadcastAlgorithm = new AtomicBroadcast();
                System.out.println("Atomic");
                break;
            case Sequential:
                broadcastAlgorithm = new SequentialBroadcast();
                System.out.println("Sequential");
                break;
            case Timestamp:
                broadcastAlgorithm = new TimestampBroadcast();
                System.out.println("Timestamp");
                break;
            default: throw new InvalidParameterException("Parameter externalInput is null");

        }
        return broadcastAlgorithm;
    }
}
