package fr.istic.strategy;

import fr.istic.clientServant.Captor;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.view.Controller;

import java.util.List;

/**
 * Interface BroadcastAlgorithm.
 *
 * @author Jimmy BRULEZ - Yann JEGU
 * @version 1.0
 */
public interface BroadcastAlgorithm {
    /**
     * Configure {@link Captor} and {@link ObserverCaptorAsync} which represents {@link fr.istic.service.Canal} for the {@link Captor}.
     * @param captor the captor.
     * @param channels the channels.
     */
    void configure(Captor captor, List<ObserverCaptorAsync> channels);

    /**
     * Execute the broadcast algorithm.
     * 3 algorithms are possible :
     * {@link fr.istic.strategy.implementation.AtomicBroadcast}.
     * This algorithm will increment the value of the captor if and only if all displayers have received the value sent by the Canal.
     * {@link fr.istic.strategy.implementation.SequentialBroadcast}.
     * This algorithm will increment the captor regardless of what value displayers have displayed.
     * {@link fr.istic.strategy.implementation.TimestampBroadcast}.
     * This algorithm is the same as {@link fr.istic.strategy.implementation.SequentialBroadcast} but it also update a timestamp (or a version number).
     */
    void execute();
}
