package fr.istic.clientServant.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.service.ObserverCaptorAsync;
import fr.istic.strategy.BroadcastAlgorithm;
import fr.istic.view.Controller;
import javafx.application.Platform;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.security.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Future;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jimmy on 22/02/17.
 */
public class CaptorImplTest {

    @InjectMocks
    CaptorImpl captor;

    @Mock
    List<ObserverCaptorAsync> channels;

    @Mock
    BroadcastAlgorithm broadcastAlgorithm;

    @Mock
    Controller ctrl;

    @Mock
    Value value;

    @Mock
    Timer timer;

    @Before
    public void setUp() throws Exception {
        this.captor = spy(new CaptorImpl(broadcastAlgorithm, ctrl));
        initMocks(this);
    }

    @Test
    public void getValue() throws Exception {
        Value v = this.captor.getValue();
        assertEquals(0, v.getValue());
        assertEquals(v, this.value);
    }

    @Test
    public void tick() throws Exception {
        //TODO
    }

    @Test
    public void attach() throws Exception {
        ObserverCaptorAsync o = mock(ObserverCaptorAsync.class);
        captor.attach(o);
        verify(channels, times(1)).add(o);
        channels.clear();

    }

    @Test
    public void detach() throws Exception {
        ObserverCaptorAsync o = mock(ObserverCaptorAsync.class);
        captor.attach(o);
        verify(channels, times(1)).add(o);
        captor.detach(o);
        verify(channels, times(1)).remove(o);
    }

}