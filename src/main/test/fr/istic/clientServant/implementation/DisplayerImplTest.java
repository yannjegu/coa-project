package fr.istic.clientServant.implementation;

import fr.istic.clientServant.Captor;
import fr.istic.service.Canal;
import fr.istic.view.Controller;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jimmy on 22/02/17.
 */
public class DisplayerImplTest {

    @InjectMocks
    DisplayerImpl displayer;

    @Mock
    Captor captor;

    @Mock
    Canal canal;

    @Mock
    ScheduledExecutorService scedule;

    @Mock
    Controller controller;

    int id;

    boolean isCalled;

    @Before
    public void setUp() throws Exception {
        this.displayer = spy(new DisplayerImpl(id, captor, scedule, controller));
        initMocks(this);
    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void getCanal() throws Exception {
        Canal c = this.displayer.getCanal();
        assertEquals(0, c.getDisplayer());
    }

    @Test
    public void isCalled() throws Exception {

    }

    @Test
    public void getCaptor() throws Exception {

    }

}